// mount_1: Raspberry Pi, RAMPS/Arduino, two 15A MOSFET boards
// mount_2: Raspberry Pi, RAMPS/Arduino, one 30A MOSFET board
// mount_3: one 30A MOSFET board to extrusion
// mount_4: RAMPS/Arduino stack to extrusion
// mount_5: Raspberry Pi to extrusion
// mount_6: RAMPS/Arduino 6mm spacer for extrusion

// 7 Jun 19: now also compatible with Re-ARM

mount_4();

module hole_test()
{
	difference()
	{
		union()
		{
			translate([-7.5,-7.5,0])
				cube([15,15,2]);
			linear_extrude(hole_t, convexity=10)
				circle(d=hole_d_m4+2*hole_wall+hole_extra);
		}
		linear_extrude(hole_t, convexity=10)
			circle(d=hole_d_m4+hole_extra);
	}
}

$fn=45;

hole_d_m2_5=2.35;
hole_d_m3=3.0;
hole_d_m4=3.75;
hole_d_m5=5.4;
hole_extra=0.05; // make longer holes a little wider
hole_wall=2; 

mb_mount_w=87;
mb_mount_h=92;
mb_mount_t=3;

arduino_w=48.3;
arduino_h1=74.9;
arduino_h2=82.6-1.27;

rpi_w=49;
rpi_h=58;

mosfet_w=42;
mosfet_h=52;

mosfet2_w=80;
mosfet2_h=58;

brace_w=4;
brace_t=3;

hole_t=8;

// frame attachment points

module frame_points()
{
	brace_l=sqrt(mb_mount_w*mb_mount_w+mb_mount_h*mb_mount_h);
	linear_extrude(brace_t, convexity=10)
	{
		rotate(acos(mb_mount_w/brace_l))
			polygon([[0,-brace_w/2],[brace_l,-brace_w/2],[brace_l,brace_w/2],[0,brace_w/2]]);
		translate([0,mb_mount_h])
		rotate(-acos(mb_mount_w/brace_l))
			polygon([[0,-brace_w/2],[brace_l,-brace_w/2],[brace_l,brace_w/2],[0,brace_w/2]]);
			
		polygon([[-brace_w/2,0],[-brace_w/2,mb_mount_h],[brace_w/2,mb_mount_h],[brace_w/2,0]]);
		polygon([[0,mb_mount_h-brace_w/2],[mb_mount_w,mb_mount_h-brace_w/2],[mb_mount_w,mb_mount_h+brace_w/2],[0,mb_mount_h+brace_w/2]]);
		polygon([[mb_mount_w-brace_w/2,0],[mb_mount_w-brace_w/2,mb_mount_h],[mb_mount_w+brace_w/2,mb_mount_h],[mb_mount_w+brace_w/2,0]]);
		polygon([[0,-brace_w/2],[mb_mount_w,-brace_w/2],[mb_mount_w,brace_w/2],[0,brace_w/2]]);
	}
					
	linear_extrude(mb_mount_t, convexity=10)
	{
		circle(d=hole_d_m3+2*hole_wall);
		translate([0,mb_mount_h])
			circle(d=hole_d_m3+2*hole_wall);
		translate([mb_mount_w,mb_mount_h])
			circle(d=hole_d_m3+2*hole_wall);
		translate([mb_mount_w,0])
			circle(d=hole_d_m3+2*hole_wall);
	}
}

module frame_drills()
{
	linear_extrude(mb_mount_t, convexity=10)
	{
		circle(d=hole_d_m3);
		translate([0,mb_mount_h])
			circle(d=hole_d_m3);
		translate([mb_mount_w,mb_mount_h])
			circle(d=hole_d_m3);
		translate([mb_mount_w,0])
			circle(d=hole_d_m3);
	}
}

// Arduino Mega & RAMPS stack attachment points

module arduino_points()
{
	linear_extrude(hole_t, convexity=10)
	{
		circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([0,arduino_h1])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([arduino_w,arduino_h2])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([arduino_w,-1.27])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([15.2,50.8])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([43.1,50.8])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);

		// for Re-ARM
		translate([-4.5,14.5])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([52.5,14.5])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([52.5,78.5])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([-4.5,78.5])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
	}

	linear_extrude(brace_t, convexity=10)
	{
		polygon([[-brace_w/2,0],[-brace_w/2,arduino_h1],[brace_w/2,arduino_h1],[brace_w/2,0]]);
		polygon([[arduino_w-brace_w/2,-1.27],[arduino_w-brace_w/2,arduino_h2],[arduino_w+brace_w/2,arduino_h2],[arduino_w+brace_w/2,-1.27]]);
		brace_l_0=sqrt(arduino_w*arduino_w+1.27*1.27);
		rotate(-acos(arduino_w/brace_l_0))
			polygon([[0,-brace_w/2],[brace_l_0,-brace_w/2],[brace_l_0,brace_w/2],[0,brace_w/2]]);
		brace_l=sqrt(arduino_w*arduino_w+(arduino_h2-arduino_h1)*(arduino_h2-arduino_h1));
		translate([0,arduino_h1])
		rotate(acos(arduino_w/brace_l))
			polygon([[0,-brace_w/2],[brace_l,-brace_w/2],[brace_l,brace_w/2],[0,brace_w/2]]);
		polygon([[0,50.8-brace_w/2],[arduino_w,50.8-brace_w/2],[arduino_w,50.8+brace_w/2],[0,50.8+brace_w/2]]);

		// for Re-ARM
		translate([-4.5,14.5])
			polygon([[0,-brace_w/2],[57,-brace_w/2],[57,brace_w/2],[0,brace_w/2]]);
		translate([-4.5,78.5])
			polygon([[0,-brace_w/2],[57,-brace_w/2],[57,brace_w/2],[0,brace_w/2]]);

	}
}

module arduino_drills()
{
	linear_extrude(hole_t, convexity=10)
	{
		circle(d=hole_d_m3+hole_extra);
		translate([0,arduino_h1])
			circle(d=hole_d_m3+hole_extra);
		translate([arduino_w,arduino_h2])
			circle(d=hole_d_m3+hole_extra);
		translate([arduino_w,-1.27])
			circle(d=hole_d_m3+hole_extra);
		translate([15.2,50.8])
			circle(d=hole_d_m3+hole_extra);
		translate([43.1,50.8])
			circle(d=hole_d_m3+hole_extra);

		// for Re-ARM
		translate([-4.5,14.5])
			circle(d=hole_d_m3+hole_extra);
		translate([52.5,14.5])
			circle(d=hole_d_m3+hole_extra);
		translate([52.5,78.5])
			circle(d=hole_d_m3+hole_extra);
		translate([-4.5,78.5])
			circle(d=hole_d_m3+hole_extra);
	}
}

// Raspberry Pi attachment points

module rpi_points()
{
	linear_extrude(hole_t, convexity=10)
	{
		circle(d=hole_d_m2_5+2*hole_wall+hole_extra);
		translate([0,rpi_h])
			circle(d=hole_d_m2_5+2*hole_wall+hole_extra);
		translate([rpi_w,rpi_h])
			circle(d=hole_d_m2_5+2*hole_wall+hole_extra);
		translate([rpi_w,0])
			circle(d=hole_d_m2_5+2*hole_wall+hole_extra);
	}

	linear_extrude(brace_t, convexity=10)
	{
		polygon([[-brace_w/2,0],[-brace_w/2,rpi_h],[brace_w/2,rpi_h],[brace_w/2,0]]);
		polygon([[rpi_w-brace_w/2,0],[rpi_w-brace_w/2,rpi_h],[rpi_w+brace_w/2,rpi_h],[rpi_w+brace_w/2,0]]);
		polygon([[0,-brace_w/2],[rpi_w,-brace_w/2],[rpi_w,brace_w/2],[0,brace_w/2]]);
		polygon([[0,rpi_h-brace_w/2],[rpi_w,rpi_h-brace_w/2],[rpi_w,rpi_h+brace_w/2],[0,rpi_h+brace_w/2]]);
	}
}

module rpi_drills() // M2.5 holes needed
{
	linear_extrude(hole_t, convexity=10)
	{
		circle(d=hole_d_m2_5+hole_extra);
		translate([0,rpi_h])
			circle(d=hole_d_m2_5+hole_extra);
		translate([rpi_w,rpi_h])
			circle(d=hole_d_m2_5+hole_extra);
		translate([rpi_w,0])
			circle(d=hole_d_m2_5+hole_extra);
	}
}

// MOSFET board attachment points

module mosfet_points()
{
	linear_extrude(hole_t, convexity=10)
	{
		circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([0,mosfet_h])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([mosfet_w,mosfet_h])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
		translate([mosfet_w,0])
			circle(d=hole_d_m3+2*hole_wall+hole_extra);
	}

	linear_extrude(brace_t, convexity=10)
	{
		polygon([[-brace_w/2,0],[-brace_w/2,mosfet_h],[brace_w/2,mosfet_h],[brace_w/2,0]]);
		polygon([[mosfet_w-brace_w/2,0],[mosfet_w-brace_w/2,mosfet_h],[mosfet_w+brace_w/2,mosfet_h],[mosfet_w+brace_w/2,0]]);
		polygon([[0,-brace_w/2],[mosfet_w,-brace_w/2],[mosfet_w,brace_w/2],[0,brace_w/2]]);
		polygon([[0,mosfet_h-brace_w/2],[mosfet_w,mosfet_h-brace_w/2],[mosfet_w,mosfet_h+brace_w/2],[0,mosfet_h+brace_w/2]]);
	}
}

module mosfet_drills()
{
	linear_extrude(hole_t, convexity=10)
	{
		circle(d=hole_d_m3+hole_extra);
		translate([0,mosfet_h])
			circle(d=hole_d_m3+hole_extra);
		translate([mosfet_w,mosfet_h])
			circle(d=hole_d_m3+hole_extra);
		translate([mosfet_w,0])
			circle(d=hole_d_m3+hole_extra);
	}
}

module mosfet2_points()
{
	linear_extrude(hole_t, convexity=10)
	{
		circle(d=hole_d_m4+2*hole_wall+hole_extra);
		translate([0,mosfet2_h])
			circle(d=hole_d_m4+2*hole_wall+hole_extra);
		translate([mosfet2_w,mosfet2_h])
			circle(d=hole_d_m4+2*hole_wall+hole_extra);
		translate([mosfet2_w,0])
			circle(d=hole_d_m4+2*hole_wall+hole_extra);
	}

	linear_extrude(brace_t, convexity=10)
	{
		polygon([[-brace_w/2,0],[-brace_w/2,mosfet2_h],[brace_w/2,mosfet2_h],[brace_w/2,0]]);
		polygon([[mosfet2_w-brace_w/2,0],[mosfet2_w-brace_w/2,mosfet2_h],[mosfet2_w+brace_w/2,mosfet2_h],[mosfet2_w+brace_w/2,0]]);
		polygon([[0,-brace_w/2],[mosfet2_w,-brace_w/2],[mosfet2_w,brace_w/2],[0,brace_w/2]]);
		polygon([[0,mosfet2_h-brace_w/2],[mosfet2_w,mosfet2_h-brace_w/2],[mosfet2_w,mosfet2_h+brace_w/2],[0,mosfet2_h+brace_w/2]]);
	}
}

module mosfet2_drills()
{
	linear_extrude(hole_t, convexity=10)
	{
		circle(d=hole_d_m4+hole_extra);
		translate([0,mosfet2_h])
			circle(d=hole_d_m4+hole_extra);
		translate([mosfet2_w,mosfet2_h])
			circle(d=hole_d_m4+hole_extra);
		translate([mosfet2_w,0])
			circle(d=hole_d_m4+hole_extra);
	}
}

// mash it all together

module mount_1()
{
	rpi_offset_x=mb_mount_w-rpi_h-15;
	rpi_offset_y=mb_mount_h+rpi_w; //mb_mount_h-rpi_h;

	arduino_offset_x=-10;
	arduino_offset_y=mb_mount_h-15; //rpi_offset_y;

	mosfet1_offset_x=mb_mount_w-mosfet_w-10;
	mosfet1_offset_y=-40;

	mosfet2_offset_x=mosfet1_offset_x-55; //  rpi_offset_x;
	mosfet2_offset_y=mosfet1_offset_y;

	difference()
	{
		union()
		{
			frame_points();
			translate([arduino_offset_x,arduino_offset_y,0])
			rotate([0,0,-90])
				arduino_points();
			translate([rpi_offset_x,rpi_offset_y,0])	
			rotate([0,0,-90])
				rpi_points();
			translate([mosfet1_offset_x,mosfet1_offset_y,0])	
				mosfet_points();
			translate([mosfet2_offset_x,mosfet2_offset_y,0])	
				mosfet_points();

			// extra bracing...may need to revise these if you move the components too much
			linear_extrude(brace_t, convexity=10)
			{
				// between MOSFETs
				polygon([[mosfet2_offset_x, mosfet2_offset_y-brace_w/2],[mosfet1_offset_x, mosfet1_offset_y-brace_w/2],[mosfet1_offset_x, mosfet1_offset_y+brace_w/2],[mosfet2_offset_x, mosfet2_offset_y+brace_w/2]]);
				
				// from mount corner to corner of MOSFET #2
				brace_l=sqrt(mosfet2_offset_x*mosfet2_offset_x+mosfet2_offset_y*mosfet2_offset_y);
				translate([mosfet2_offset_x,mosfet2_offset_y])
				rotate(acos(abs(mosfet2_offset_x/brace_l)))
					polygon([[0,-brace_w/2],[brace_l,-brace_w/2],[brace_l,brace_w/2],[0,brace_w/2]]);
					
				// from mount corners to Raspberry Pi
				brace_l_1=sqrt((mb_mount_w-rpi_offset_x-rpi_h)*(mb_mount_w-rpi_offset_x-rpi_h)+(rpi_w)*(rpi_w));
				translate([mb_mount_w,mb_mount_h])
				rotate(90+asin((mb_mount_w-rpi_offset_x-rpi_h)/brace_l_1))
					polygon([[0,-brace_w/2],[brace_l_1,-brace_w/2],[brace_l_1,brace_w/2],[0,brace_w/2]]);
				brace_l_2=sqrt(rpi_offset_x*rpi_offset_x+(rpi_offset_y-mb_mount_h)*(rpi_offset_y-mb_mount_h));
				translate([0,mb_mount_h])
				rotate(asin((rpi_offset_y-mb_mount_h)/brace_l_2))
					polygon([[0,-brace_w/2],[brace_l_2,-brace_w/2],[brace_l_2,brace_w/2],[0,brace_w/2]]);
				brace_l_3=sqrt((mb_mount_w-rpi_offset_x)*(mb_mount_w-rpi_offset_x)+(mb_mount_h-rpi_offset_y)*(mb_mount_h-rpi_offset_y));
				translate([rpi_offset_x,rpi_offset_y])
				rotate(-acos((mb_mount_w-rpi_offset_x)/brace_l_3))
					polygon([[0,-brace_w/2],[brace_l_3,-brace_w/2],[brace_l_3,brace_w/2],[0,brace_w/2]]);
			}
		}
		
		union()
		{
			frame_drills();
			translate([arduino_offset_x,arduino_offset_y,0])
			rotate([0,0,-90])
				arduino_drills();
			translate([rpi_offset_x,rpi_offset_y,0])	
			rotate([0,0,-90])
				rpi_drills();
			translate([mosfet1_offset_x,mosfet1_offset_y,0])	
				mosfet_drills();
			translate([mosfet2_offset_x,mosfet2_offset_y,0])	
				mosfet_drills();
		}
	}
}

module mount_2()
{
	rpi_offset_x=mb_mount_w-rpi_h-15;
	rpi_offset_y=mb_mount_h+rpi_w; //mb_mount_h-rpi_h;

	arduino_offset_x=-10;
	arduino_offset_y=mb_mount_h-15; //rpi_offset_y;

	mosfet_offset_x=(mb_mount_w-mosfet2_w)/2-25;
	mosfet_offset_y=-mosfet2_h;

	difference()
	{
		union()
		{
			frame_points();
			translate([arduino_offset_x,arduino_offset_y,0])
			rotate([0,0,-90])
				arduino_points();
			translate([rpi_offset_x,rpi_offset_y,0])	
			rotate([0,0,-90])
				rpi_points();
			translate([mosfet_offset_x,mosfet_offset_y,0])	
				mosfet2_points();

			// extra bracing...may need to revise these if you move the components too much
			linear_extrude(brace_t, convexity=10)
			{
				// from mount corners to Raspberry Pi
				brace_l_1=sqrt((mb_mount_w-rpi_offset_x-rpi_h)*(mb_mount_w-rpi_offset_x-rpi_h)+(rpi_w)*(rpi_w));
				translate([mb_mount_w,mb_mount_h])
				rotate(90+asin((mb_mount_w-rpi_offset_x-rpi_h)/brace_l_1))
					polygon([[0,-brace_w/2],[brace_l_1,-brace_w/2],[brace_l_1,brace_w/2],[0,brace_w/2]]);
				brace_l_2=sqrt(rpi_offset_x*rpi_offset_x+(rpi_offset_y-mb_mount_h)*(rpi_offset_y-mb_mount_h));
				translate([0,mb_mount_h])
				rotate(asin((rpi_offset_y-mb_mount_h)/brace_l_2))
					polygon([[0,-brace_w/2],[brace_l_2,-brace_w/2],[brace_l_2,brace_w/2],[0,brace_w/2]]);
				brace_l_3=sqrt((mb_mount_w-rpi_offset_x)*(mb_mount_w-rpi_offset_x)+(mb_mount_h-rpi_offset_y)*(mb_mount_h-rpi_offset_y));
				translate([rpi_offset_x,rpi_offset_y])
				rotate(-acos((mb_mount_w-rpi_offset_x)/brace_l_3))
					polygon([[0,-brace_w/2],[brace_l_3,-brace_w/2],[brace_l_3,brace_w/2],[0,brace_w/2]]);
			}
		}
		
		union()
		{
			frame_drills();
			translate([arduino_offset_x,arduino_offset_y,0])
			rotate([0,0,-90])
				arduino_drills();
			translate([rpi_offset_x,rpi_offset_y,0])	
			rotate([0,0,-90])
				rpi_drills();
			translate([mosfet_offset_x,mosfet_offset_y,0])	
				mosfet2_drills();
		}
	}
}

module mount_3()
{
	difference()
	{
		union()
		{
			mosfet2_points();

			translate([-brace_w/2,-brace_w/2,0])
			cube([mosfet2_w+brace_w,20+brace_w,4]);
		}

		union()
		{
			mosfet2_drills();

			translate([mosfet2_w/4,10,0])
				cylinder(d=hole_d_m5, h=4);
			translate([mosfet2_w*3/4,10,0])
				cylinder(d=hole_d_m5, h=4);
		}
	}
}

module mount_4()
{
	difference()
	{
		union()
		{
			translate([-45,0,0])
			arduino_points();

			translate([arduino_w-15-brace_w/2-35,-brace_w*3/4,0])
				cube([35+20+brace_w,arduino_h2+brace_w,4]);
		}

		union()
		{
			translate([-45,0,0])
			arduino_drills();

			translate([arduino_w-5,3*arduino_h2/4,0])
				cylinder(d=hole_d_m5, h=4);
			translate([arduino_w-5,arduino_h2/4,0])
				cylinder(d=hole_d_m5, h=4);
		}
	}
}

module mount_5()
{
	difference()
	{
		union()
		{
			rpi_points();

			translate([rpi_w-20-brace_w/2,-brace_w/2,0])
				cube([20+brace_w,rpi_h+brace_w,4]);
		}

		union()
		{
			rpi_drills();

			translate([rpi_w-10,3*rpi_h/4,0])
				cylinder(d=hole_d_m5, h=4);
			translate([rpi_w-10,rpi_h/4,0])
				cylinder(d=hole_d_m5, h=4);
		}
	}
}

module mount_6()
{
	difference()
	{
		cube([20+arduino_h2/2,20,7]);
		translate([10,10,0])
			cylinder(d=4.7, h=7);
		translate([10+arduino_h2/2,10,0])
			cylinder(d=4.7, h=7);
		translate([10+arduino_h2/8,10,0])
		{
			cylinder(d=5.4, h=7);
			translate([0,0,4])
				cylinder(d=12, h=3);
		}
		translate([10+3*arduino_h2/8,10,0])
		{
			cylinder(d=5.4, h=7);
			translate([0,0,4])
				cylinder(d=12, h=3);
		}
	}

}